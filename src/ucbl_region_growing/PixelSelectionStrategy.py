class PixelSelectionStrategy(object):
    class Threshold(object):
        def __init__(self, comparison_heuristic, threshold):
            self.comparision_heuristic = comparison_heuristic
            self.threshold = threshold

        def __call__(self, pa, pb):
            return self.comparision_heuristic(pa, pb) < self.threshold
