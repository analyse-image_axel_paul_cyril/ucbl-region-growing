import numpy as np


class Region(object):
    def __init__(self, src, dst, label, pixel_selector, neighbor_generator, seed):
        self.src = src
        self.dst = dst
        self.label = label
        self.pixel_selector = pixel_selector
        self.neighbor_generator = neighbor_generator
        self.opens = {seed}
        self.pixels = {seed}
        self.borders = set()

        self.avg_up_to_date = False
        self.avg = None

        dst[seed[0]][seed[1]] = label

    def grow(self):
        if len(self.opens) == 0:
            return 0

        new_opens = set()
        for p in self.opens:
            for n in self.neighbor_generator(p):
                a = self.dst[n[0]][n[1]]
                if a == 0:
                    if self.pixel_selector(self.src[p[0]][p[1]], self.src[n[0]][n[1]]):
                        self.dst[p[0]][p[1]] = self.label
                        new_opens.add(n)
                elif a != self.label:
                    self.borders.add(a)

        self.avg_up_to_date = False
        self.opens = new_opens
        self.pixels = self.pixels.union(self.opens)

        return len(self.opens)

    def merge(self, other):
        for p in other.pixels:
            self.dst[p[0]][p[1]] = self.label

        self.pixels = self.pixels.union(other.pixels)
        self.avg_up_to_date = False

    def merge_borders(self, to_label, from_label):
        try:
            self.borders.remove(from_label)
            if self.label != to_label:
                self.borders.add(to_label)
        except KeyError:
            pass

    def remap_labels(self, lbl_map):
        self.label = lbl_map[self.label]
        self.borders = {lbl_map[b] for b in self.borders}

        for p in self.pixels:
            self.dst[p[0]][p[1]] = self.label

    def get_avg(self):
        if not self.avg_up_to_date:
            self.avg = np.average([self.src[c[0]][c[1]] for c in self.pixels], axis=0)
            self.avg_up_to_date = True
        return self.avg

