import numpy as np


class RegionMergingAlgorithm(object):
    class NRegions(object):
        def __init__(self, count, difference_heuristic):
            self.count = count
            self.difference_heuristic = difference_heuristic

        def __call__(self, regions):
            new_regions = list(regions)

            while len(new_regions) > self.count:
                smallest_left = None
                smallest_right = None
                smallest_diff = np.inf

                for i, rl in enumerate(new_regions[:-1]):
                    for rr in new_regions[i:]:
                        if rl.label not in rr.borders and rr.label not in rl.borders:
                            continue  # Do not merge non-adjacent regions

                        diff = self.difference_heuristic(rl, rr)
                        if diff < smallest_diff:
                            smallest_diff = diff
                            smallest_left = rl
                            smallest_right = rr

                if smallest_left is None or smallest_right is None:
                    print("Could not find any new region to merge. Stopping at {} regions.".format(len(new_regions)))
                    break

                smallest_left.merge(smallest_right)

                new_regions.remove(smallest_right)

                for r in new_regions:
                    if r != smallest_right:
                        r.merge_borders(smallest_left.label, smallest_right.label)

            return new_regions

    class DiffThreshold(object):
        def __init__(self, difference_heuristic, threshold):
            self.difference_heuristic = difference_heuristic
            self.threshold = threshold

        def __call__(self, regions):
            new_regions = list(regions)

            while True:
                smallest_left = None
                smallest_right = None
                smallest_diff = np.inf

                for i, rl in enumerate(new_regions[:-1]):
                    for rr in new_regions[i:]:
                        if rl.label not in rr.borders and rr.label not in rl.borders:
                            continue  # Do not merge non-adjacent regions

                        diff = self.difference_heuristic(rl, rr)
                        if diff < smallest_diff:
                            smallest_diff = diff
                            smallest_left = rl
                            smallest_right = rr

                if smallest_left is None or smallest_right is None or smallest_diff >= self.threshold:
                    break

                smallest_left.merge(smallest_right)

                new_regions.remove(smallest_right)

                for r in new_regions:
                    if r != smallest_right:
                        r.merge_borders(smallest_left.label, smallest_right.label)

            return new_regions
