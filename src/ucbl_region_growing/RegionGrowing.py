import numpy as np
from .Region import Region


class RegionGrowing(object):
    def __init__(self, src, pixel_comparator, coord_seeds):
        self.src = src
        self.dst = np.zeros((src.shape[0], src.shape[1]), dtype=np.int16)  # TODO Init  int[w][h][1] array filled with None

        self.regions = [Region(src, self.dst, i + 1, pixel_comparator, self.generate_neighbors, seed)
                        for i, seed in enumerate(coord_seeds)]

    def generate_neighbors(self, pc):
        w, h = pc
        neighbors = []

        if w > 0:
            neighbors.append((w - 1, h))
        if w < self.src.shape[0] - 1:
            neighbors.append((w + 1, h))
        if h > 0:
            neighbors.append((w, h - 1))
        if h < self.src.shape[1] - 1:
            neighbors.append((w, h + 1))

        return neighbors

    def run(self):
        iteration = 0
        while any([r.grow() != 0 for r in self.regions]):
            print("Growing iteration {}".format(iteration))
            iteration += 1
            pass

    def merge(self, merging_algorithm):
        self.regions = merging_algorithm(self.regions)
        pass

    def order_labels(self):
        lbl_map = {r.label: i for i, r in enumerate(self.regions)}
        for r in self.regions:
            r.remap_labels(lbl_map)
