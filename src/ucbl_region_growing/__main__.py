import sys, getopt
import cv2

from ucbl_region_growing import SeedPicker
from ucbl_region_growing import RegionGrowing
from ucbl_region_growing import RegionMergingAlgorithm
from ucbl_region_growing import PixelDifferenceHeuristic
from ucbl_region_growing import PixelSelectionStrategy
from ucbl_region_growing import RegionDifferenceHeuristic
from ucbl_region_growing import Label2Color
from ucbl_region_growing import labels_to_image


def usage():
    print("Usage: \n -i, --input     select a file as input (image or video). \n -o, --output\
    the recipient file. \n --pixel-difference-heuristic     select an pixel difference selection heuristic between  those parameters Maximum Average and Magnitude. \n\n\
 --pixel-selection-strategy    Specify the treshold between 0 and 1. \n --region-merging-algorithm     merges the pair of related regions that is most relevant according to the evaluation of the difference heuristic on their average pixels.\
 Algorithm : \n        Treshold(t) : As long as this evaluation is less than \"t\" \n        NRegions(n) : As long as more than \"n\" regions remains.  \n\n\
example 1: python3 -m ucbl_region_growing -i input.png -o output.png --pixel-difference-heuristic=Magnitude --pixel-selection-strategy=\"Threshold(5/255)\" --region-merging-algorithm=\"NRegions(6)\"\n \n \
example 2: python3 -m ucbl_region_growing -i input.png -o output.png --pixel-difference-heuristic=Magnitude --pixel-selection-strategy=\"Threshold(5/255)\" --region-merging-algorithm=\"Threshold(10/255)\" ")

def read_args():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hg:i:o:d", ["help", "input=", "pixel-difference-heuristic=", "pixel-selection-strategy=", "region-merging-algorithm=", "output="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    input_file_arg = None
    output_file_arg = None
    pix_diff_heuristic_arg = None
    pix_sel_strategy_arg = None
    reg_merg_alg_arg = None

    for op, arg in opts:
        if op in ("-h", "--help"):
            usage()
            sys.exit()
        elif op == '-d':
            global _debug
            _debug = 1
        elif op in ("-i", "--input"):
            input_file_arg = arg
        elif op in ("-o", "--output"):
            output_file_arg = arg
        elif op == "--pixel-difference-heuristic":
            pix_diff_heuristic_arg = arg
        elif op == "--pixel-selection-strategy":
            pix_sel_strategy_arg = arg
        elif op == "--region-merging-algorithm":
            reg_merg_alg_arg = arg

    return input_file_arg, output_file_arg, pix_diff_heuristic_arg, pix_sel_strategy_arg, reg_merg_alg_arg, args


def main():
    if_arg, of_arg, pdh_arg, pss_arg, rmm_arg, args = read_args()

    if if_arg is None:
        raise AttributeError("Missing input (-i <input path>)/(--input=<input path>) argument")
    if of_arg is None:
        raise AttributeError("Missing output (-o <output path>)/(--output=<output path>) argument")

    img = cv2.imread(if_arg)
    if img is None:
        sys.exit("Could not read the image.")

    if pdh_arg is None:
        raise AttributeError("Missing argument: --pixel-difference-heuristic=[Maximum/Average/Magnitude]")
    pix_diff_heur = PixelDifferenceHeuristic.factory(pdh_arg)

    if pss_arg is None:
        raise AttributeError("Missing argument: --pixel-selection-strategy=Threshold(t)\n With 0 <= t <= 1")

    pss_arg = list(pss_arg.replace(")", "").split("("))
    if pss_arg[0].lower() != "threshold":
        raise NotImplementedError("The pixel selection strategy \"{}\" is not implemented. The only strategy available yet is \"Threshold(t)\"".format(pss_arg[0]))

    pix_threshold = float(eval(pss_arg[1]))
    if not 0.0 <= pix_threshold <= 1.0:
        raise AttributeError("The pixel difference threshold must be contained between 0 and 1. Currently {}".format(pix_threshold))

    print("Selecting seeds. Press any key to continue...")

    sp = SeedPicker(img)
    seeds = sp.pick()

    pix_selection_strat = PixelSelectionStrategy.Threshold(pix_diff_heur, pix_threshold)
    rg = RegionGrowing(img, pix_selection_strat, seeds)
    rg.run()

    if rmm_arg is not None:
        rms_arg = list(rmm_arg.replace(")", "").split("("))
        if rms_arg[0].lower() == "nregions":
            rc = int(eval(rms_arg[1]))
            if not 0 < rc:
                raise AttributeError("The objective region count of the NRegion strategy must be strictly positive. Currently: {}".format(rms_arg))

            rm = RegionMergingAlgorithm.NRegions(rc, RegionDifferenceHeuristic.AvgPixel(pix_diff_heur))
        elif rms_arg[0].lower() == "threshold":
            reg_threshold = float(eval(pss_arg[1]))
            if not 0.0 <= reg_threshold <= 1.0:
                raise AttributeError("The region difference threshold must be contained between 0 and 1. Currently {}".format(reg_threshold))

            rm = RegionMergingAlgorithm.DiffThreshold(RegionDifferenceHeuristic.AvgPixel(pix_diff_heur), 5 / 255)
        else:
            raise NotImplementedError("The merging strategy {} is not implemented yet".format(rms_arg[0]))

        rg.merge(rm)
        rg.order_labels()

    n = len(rg.regions) + 1
    lm = Label2Color(n)

    segmented_image = labels_to_image(rg.dst, lm)

    cv2.imwrite(of_arg, segmented_image)


if __name__ == "__main__":
    # execute only if run as a script
    main()
