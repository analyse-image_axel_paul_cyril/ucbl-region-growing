import sys
import math
import numpy as np


class PixelDifferenceHeuristic(object):
    class Maximum(object):
        def __call__(self, pa, pb):
            return np.abs(np.subtract(pa, pb)).max() / 255

    class Average(object):
        def __call__(self, pa, pb):
            return np.average(np.abs(np.subtract(pa, pb))) / 255

    class Magnitude(object):
        def __call__(self, pa, pb):
            return np.linalg.norm(np.subtract(pa, pb)) / math.sqrt((255 ** 2) * 3)

    @staticmethod
    def factory(arg):
        if arg.lower() in ("maximum", "max"):
            return PixelDifferenceHeuristic.Maximum()
        elif arg.lower() in ("average", "avg"):
            return PixelDifferenceHeuristic.Average()
        elif arg.lower() in ("magnitude", "mag"):
            return PixelDifferenceHeuristic.Magnitude()
