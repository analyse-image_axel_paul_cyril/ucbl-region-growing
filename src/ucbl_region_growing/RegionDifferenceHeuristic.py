class RegionDifferenceHeuristic(object):
    class AvgPixel(object):
        def __init__(self, pixel_difference):
            self.pixel_difference = pixel_difference

        def __call__(self, ra, rb):
            return self.pixel_difference(ra.get_avg(), rb.get_avg())
