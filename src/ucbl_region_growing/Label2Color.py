import numpy as np


primes = np.array([2731, 2803, 2897])


class Label2Color(object):
    def __init__(self, n):
        self.n = n

    def __call__(self, i):
        if i > self.n:
            raise KeyError("Label {} greater or equal than map size: {}".format(i, self.n))

        if i == 0:
            return np.zeros(3)
        else:
            mult = np.multiply(primes, i)
            res = np.mod(mult, 255)
            return np.mod(np.multiply(primes, i), 255)


def labels_to_image(labels, label_color_map):
    return np.array([[label_color_map(l) for l in line] for line in labels])
