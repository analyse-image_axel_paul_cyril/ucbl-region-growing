import numpy as np
import cv2


class SeedPicker(object):
    def __init__(self, img):
        self.img = np.copy(img)
        self.seeds = set()

    def pick(self):
        cv2.namedWindow('Press any key')
        cv2.imshow('Press any key', self.img)
        cv2.setMouseCallback('Press any key', self._draw_circle)
        cv2.waitKey()
        cv2.destroyWindow("Press any key")
        return self.seeds

    def _draw_circle(self, event,x,y,flags,param):
        if event == cv2.EVENT_LBUTTONDOWN:
            self.seeds.add((y, x))
            cv2.circle(self.img, (x, y), 5, (255, 0, 0), -1)
            cv2.imshow('Press any key', self.img)
