# UCBL-REGION-GROWING PACKAGE
Region growing is a simple region-based image segmentation method. It is also classified as a pixel-based image segmentation method since it involves the selection of initial seed points.
This approach to segmentation examines neighboring pixels of initial seed points and determines whether the pixel neighbors should be added to the region.


In this package, you will find:

The class Label2Color which attribute a distinct color for each region

The class PixelDifferenceHeuristic which quantify the difference between 2 pixels.

The class RegionDifferenceHeuristic which quantify the difference between 2 regions.

The class SeedPicker allow us to put seed manually on the picture.

The class RegionGrowing which contain our algorithm of region growing.

The class RegionMergingAlgorithm which merge regions that are judge close enough by RegionDifferenceHeuristic.


The main.py file is the entry point of the program. It parses command arguments, and [load => filter => write] a video file accordingly.

Accordingly to this package, we provides some usage of this package :

`python3 -m ucbl_region_growing -i input.png -o output.png --pixel-difference-heuristic=Magnitude --pixel-selection-strategy="Threshold(5/255)" --region-merging-algorithm="Threshold(10/255)"`


We can have anothers examples:

`python3 -m ucbl_region_growing -i input.png -o output.png --pixel-difference-heuristic=Maximum --pixel-selection-strategy="Threshold(5/255)"`

`python3 -m ucbl_region_growing -i input.png -o output.png --pixel-difference-heuristic=Magnitude --pixel-selection-strategy="Threshold(5/255)" --region-merging-algorithm="NRegions(6)"`

After you plant the seeds, press any keys to calculate the result.


