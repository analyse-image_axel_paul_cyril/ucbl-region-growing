import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="ucbl-region-growing",
    version="0.0.1",
    author="Axel Paccalin and", # TODO Add name
    author_email="axel.paccalin@etu.univ-lyon1.fr and ", # TODO Add email
    description="An image region growing software",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://forge.univ-lyon1.fr/analyse-image_axel_paul/ucbl-region-growing",
    project_urls={
        "Bug Tracker": "https://forge.univ-lyon1.fr/analyse-image_axel_paul/ucbl-region-growing/issues",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    python_requires=">=3.6",
    install_requires="ucbl-video-io>=0.0.1",
)